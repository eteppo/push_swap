/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   checker.h                                          :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/05/24 11:06:28 by eteppo        #+#    #+#                 */
/*   Updated: 2021/08/09 13:06:32 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef CHECKER_H
# define CHECKER_H

# include "../libft/libft.h"
# include "../includes/shared.h"

void	order_checking(t_list *a, t_list *b);
int		instruction_router(char *line, t_list **a, t_list **b);
int		main(int argc, char **argv);

void	ft_swap_one_stack(t_list *stack);
void	ft_swap_two_stacks(t_list *stack_a, t_list *stack_b);
void	ft_push_stack(t_list **dst, t_list **src);

void	ft_rotate(t_list *stack);
void	ft_rrotate(t_list *stack);
void	ft_rotate_stacks(t_list *a, t_list *b, char *str);

#endif 