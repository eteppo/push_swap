/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   shared.h                                           :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/08/09 12:41:34 by eteppo        #+#    #+#                 */
/*   Updated: 2021/08/09 13:05:36 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef SHARED_H
# define SHARED_H

# include "../libft/libft.h"

int		argument_checker(int argc, char **argv);
int		save_stack(int argc, char **argv, t_list **a);

void	ft_error(void);
void	delete_list(t_list *stack);
int		is_in_order(t_list *a);
void	ft_swap(int *a, int *b);
int		ft_atoi_mod(const char *str);

#endif 
