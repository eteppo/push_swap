/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   push_swap.h                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/05/24 11:06:28 by eteppo        #+#    #+#                 */
/*   Updated: 2021/08/11 09:56:51 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include "../libft/libft.h"
# include "../includes/shared.h"

typedef struct s_nbs
{
	int	bool_push;
	int	nb2push;
	int	a1;
	int	a2;
	int	a3;
	int	b;
	int	location_a1;
	int	location_a2;
	int	location_a3;
	int	location_b;
	int	direction_a;
	int	direction_b;
}				t_nbs;

typedef struct s_nodes
{
	int	nb1;
	int	nb2;
	int	nb3;
}		t_nodes;

int		two_arguments(t_list *a);
int		three_arguments(t_list *a);
void	thousand_arguments(t_list *a, t_list *b);
void	listing_instructions(t_list *a, t_list *b);
int		main(int argc, char **argv);

void	initialize_node_values(t_list *a, t_nodes *node);
void	initialize_nbs(t_nbs *nb);
int		calculate_chunk_size(t_list *stack);
int		find_max(t_list *stack);
int		find_min(t_list *stack);

int		find_min_and_loc(t_list *stack, int *value);
int		calculate_cost(int location, int size);
void	assign_small_nb_and_loc_to_a1(t_nbs *nb, int small_nb, int loc);
int		choose_most_cost_efficient_loc(t_nbs *nb, int size, int *nb2push);
int		find_small_nb_and_loc_in_chunk(t_list *a, t_nbs *nb, int *nb2push);

void	assign_small_nb_and_loc_to_a2(t_nbs *nb, int small_nb, int loc);
void	assign_small_nb_and_loc_to_a3(t_nbs *nb, int small_nb, int loc);

int		find_direction_to_top(t_list *stack, int location);
void	move_to_biggest_to_top(t_list *b);
void	move_stacks(t_list *a, t_list *b, char *instruction);
void	update_location(t_nbs *nb, int *location);
void	step_to_direction(t_list *a, t_list *b, t_nbs *nb, int *loc);

int		find_closest_but_smaller_nb(t_list *stack, int nb2push);
int		find_max_and_loc(t_list *stack, int *value);
int		is_correct_spot_to_push(t_list *b, int nb2push);
int		find_spot_to_push(t_list *stack, int nb2push);

void	rotate_and_write(t_list *stack, char *instruction, int bool_write);
void	rev_rotate_and_write(t_list *stack, char *instruction, int bool_write);
void	swap_and_write(t_list *stack, char *instruction);
void	push_and_write(t_list **dst, t_list **src, char *instruction);
int		push_if_correct_spot(t_list **a, t_list **b, int location);

#endif 