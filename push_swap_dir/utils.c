/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   utils.c                                            :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/05/19 17:29:17 by eteppo        #+#    #+#                 */
/*   Updated: 2021/08/11 08:59:18 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	initialize_node_values(t_list *a, t_nodes *node)
{
	node->nb1 = *((int *)a->content);
	node->nb2 = *((int *)a->next->content);
	node->nb3 = *((int *)a->next->next->content);
}

void	initialize_nbs(t_nbs *nb)
{
	nb->bool_push = 0;
	nb->nb2push = 0;
	nb->a1 = 2147483647;
	nb->a2 = 2147483647;
	nb->a3 = 2147483647;
	nb->b = 2147483647;
	nb->location_a1 = 2147483647;
	nb->location_a2 = 2147483647;
	nb->location_a3 = 2147483647;
	nb->location_b = -1;
	nb->direction_a = 0;
	nb->direction_b = 0;
}

int	calculate_chunk_size(t_list *stack)
{
	int	divider;

	if (ft_lstsize(stack) <= 56)
		divider = 1;
	else if (ft_lstsize(stack) <= 150)
		divider = ft_lstsize(stack) / 10;
	else if (ft_lstsize(stack) <= 300)
		divider = ft_lstsize(stack) / 30;
	else if (ft_lstsize(stack) <= 400)
		divider = ft_lstsize(stack) / 35;
	else
		divider = ft_lstsize(stack) / 40;
	return (divider);
}

int	find_max(t_list *stack)
{
	int		max;

	max = 0;
	if (stack)
	{
		max = *((int *)stack->content);
		while (stack)
		{
			if (max < *((int *)stack->content))
				max = *((int *)stack->content);
			stack = stack->next;
		}
	}
	return (max);
}

int	find_min(t_list *stack)
{
	int		min;

	min = 0;
	if (stack)
	{
		min = *((int *)stack->content);
		while (stack)
		{
			if (min > *((int *)stack->content))
				min = *((int *)stack->content);
			stack = stack->next;
		}
		return (min);
	}
	return (-1);
}
