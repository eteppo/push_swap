/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   main.c                                             :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/05/13 13:28:45 by eteppo        #+#    #+#                 */
/*   Updated: 2021/08/11 09:21:40 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

int	two_arguments(t_list *a)
{
	int	*first;
	int	*second;

	first = (int *)a->content;
	second = (int *)a->next->content;
	if (*first > *second)
		swap_and_write(a, "sa\n");
	return (0);
}

int	three_arguments(t_list *a)
{
	t_nodes	node;

	if (ft_lstsize(a) == 2)
		return (two_arguments(a));
	initialize_node_values(a, &node);
	if (node.nb1 < node.nb2 && node.nb2 > node.nb3 && node.nb3 > node.nb1)
	{
		swap_and_write(a, "sa\n");
		rotate_and_write(a, "ra\n", 1);
	}
	else if (node.nb1 < node.nb2 && node.nb2 > node.nb3 && node.nb3 < node.nb1)
		rev_rotate_and_write(a, "rra\n", 1);
	else if (node.nb1 > node.nb2 && node.nb2 < node.nb3 && node.nb3 > node.nb1)
		swap_and_write(a, "sa\n");
	else if (node.nb1 > node.nb2 && node.nb2 < node.nb3 && node.nb3 < node.nb1)
		rotate_and_write(a, "ra\n", 1);
	else if (node.nb1 > node.nb2 && node.nb2 > node.nb3 && node.nb3 < node.nb1)
	{
		rotate_and_write(a, "ra\n", 1);
		swap_and_write(a, "sa\n");
	}
	return (0);
}

void	thousand_arguments(t_list *a, t_list *b)
{
	t_nbs	nb;
	int		location;

	while (ft_lstsize(a) > 3)
	{
		initialize_nbs(&nb);
		if (ft_lstsize(a) > 5)
			location = find_small_nb_and_loc_in_chunk(a, &nb, &nb.nb2push);
		else
			location = find_min_and_loc(a, &nb.nb2push);
		while (nb.bool_push != 1)
		{
			nb.direction_a = find_direction_to_top(a, location);
			nb.location_b = find_spot_to_push(b, nb.nb2push);
			nb.direction_b = find_direction_to_top(b, nb.location_b);
			nb.bool_push = push_if_correct_spot(&a, &b, location);
			if (nb.bool_push == 0)
				step_to_direction(a, b, &nb, &location);
		}
	}
	move_to_biggest_to_top(b);
	three_arguments(a);
	while (ft_lstsize(b))
		push_and_write(&a, &b, "pa\n");
}

void	listing_instructions(t_list *a, t_list *b)
{
	if (ft_lstsize(a) < 2)
		return ;
	else if (ft_lstsize(a) <= 3)
		three_arguments(a);
	else
		thousand_arguments(a, b);
}

int	main(int argc, char **argv)
{
	t_list		*a;
	t_list		*b;

	a = NULL;
	b = NULL;
	if (argument_checker(argc, argv) == -1)
		ft_error();
	else if (argument_checker(argc, argv) == 0)
		return (0);
	if (save_stack(argc, argv, &a) == -1)
		ft_error();
	if (is_in_order(a) == 0)
		listing_instructions(a, b);
	delete_list(a);
	delete_list(b);
	return (0);
}
