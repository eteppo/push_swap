/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   find_spot_to_push.c                                :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/07/27 12:15:13 by eteppo        #+#    #+#                 */
/*   Updated: 2021/07/30 16:12:45 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

int	find_closest_but_smaller_nb(t_list *stack, int nb2push)
{
	int				location;
	int				i;
	long long int	current_diff;
	long long int	smallest_diff;			

	location = -1;
	i = 0;
	smallest_diff = 4294967295;
	while (stack)
	{
		current_diff = nb2push - *((int *)stack->content);
		if (current_diff < smallest_diff && current_diff > 0)
		{
			smallest_diff = current_diff;
			location = i;
		}
		i++;
		stack = stack->next;
	}
	return (location);
}

int	find_max_and_loc(t_list *stack, int *value)
{
	int			location;
	int			i;
	int			max;

	location = ft_lstsize(stack) / 2;
	i = 0;
	max = -2147483648;
	while (stack)
	{
		if ((*((int *)stack->content) >= max))
		{
			max = *((int *)stack->content);
			location = i;
		}
		i++;
		stack = stack->next;
	}
	*value = max;
	return (location);
}

int	is_correct_spot_to_push(t_list *b, int nb2push)
{
	t_list	*last_node;
	int		top;
	int		bottom;

	if (ft_lstsize(b) > 1)
	{
		last_node = ft_lstlast(b);
		top = *((int *)b->content);
		bottom = *((int *)last_node->content);
		if (nb2push < top && nb2push < bottom && bottom < top)
			return (1);
		else if (nb2push > top && nb2push > bottom && bottom < top)
			return (1);
		else if (nb2push > top && nb2push < bottom && bottom > top)
			return (1);
		return (0);
	}
	return (1);
}

int	find_spot_to_push(t_list *stack, int nb2push)
{
	int	location;
	int	min;
	int	max;

	location = 0;
	min = find_min(stack);
	max = find_max(stack);
	if (is_correct_spot_to_push(stack, nb2push) == 0)
	{
		if (nb2push < min || nb2push > max)
			location = find_max_and_loc(stack, &min);
		else
			location = find_closest_but_smaller_nb(stack, nb2push);
	}
	return (location);
}
