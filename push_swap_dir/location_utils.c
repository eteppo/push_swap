/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   location_utils.c                                   :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/08/11 09:09:01 by eteppo        #+#    #+#                 */
/*   Updated: 2021/08/11 09:09:32 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	assign_small_nb_and_loc_to_a2(t_nbs *nb, int small_nb, int loc)
{
	nb->a3 = nb->a2;
	nb->location_a3 = nb->location_a2;
	nb->a2 = small_nb;
	nb->location_a2 = loc;
}

void	assign_small_nb_and_loc_to_a3(t_nbs *nb, int small_nb, int loc)
{
	nb->a3 = small_nb;
	nb->location_a3 = loc;
}
