/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   find_location.c                                    :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/06/03 14:00:43 by eteppo        #+#    #+#                 */
/*   Updated: 2021/08/11 11:05:22 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

int	find_min_and_loc(t_list *stack, int *value)
{
	int			location;
	int			i;
	int			min;

	location = ft_lstsize(stack) / 2;
	i = 0;
	min = 2147483647;
	while (stack)
	{
		if ((*((int *)stack->content) <= min))
		{
			min = *((int *)stack->content);
			location = i;
		}
		i++;
		stack = stack->next;
	}
	*value = min;
	return (location);
}

int	calculate_cost(int location, int size)
{
	int	cost;

	if (location > (size / 2) && size < 56)
		cost = size - location;
	else
		cost = location;
	return (cost);
}

int	choose_most_cost_efficient_loc(t_nbs *nb, int size, int *nb2push)
{
	int	cost1;
	int	cost2;
	int	cost3;
	int	location;

	cost1 = calculate_cost(nb->location_a1, size);
	cost2 = calculate_cost(nb->location_a2, size);
	cost3 = calculate_cost(nb->location_a3, size);
	location = nb->location_a1;
	*nb2push = nb->a1;
	if (cost2 < cost1 && cost2 < cost3)
	{
		location = nb->location_a2;
		*nb2push = nb->a2;
	}
	if (cost3 < cost1 && cost3 < cost2)
	{
		location = nb->location_a3;
		*nb2push = nb->a3;
	}
	return (location);
}

void	assign_small_nb_and_loc_to_a1(t_nbs *nb, int small_nb, int loc)
{
	nb->a3 = nb->a2;
	nb->location_a3 = nb->location_a2;
	nb->a2 = nb->a1;
	nb->location_a2 = nb->location_a1;
	nb->a1 = small_nb;
	nb->location_a1 = loc;
}

int	find_small_nb_and_loc_in_chunk(t_list *a, t_nbs *nb, int *nb2push)
{
	int		i;
	int		divider;
	int		size;

	i = 0;
	size = ft_lstsize(a);
	divider = calculate_chunk_size(a);
	while (i * divider < size)
	{
		if (nb->a1 >= *((int *)a->content))
			assign_small_nb_and_loc_to_a1(nb, *((int *)a->content), i);
		if (nb->a2 > *((int *)a->content) && nb->a1 != *((int *)a->content))
			assign_small_nb_and_loc_to_a2(nb, *((int *)a->content), i);
		if (nb->a3 > *((int *)a->content) && nb->a1 != *((int *)a->content) && \
			nb->a2 != *((int *)a->content))
			assign_small_nb_and_loc_to_a3(nb, *((int *)a->content), i);
		i++;
		a = a->next;
	}
	return (choose_most_cost_efficient_loc(nb, size, nb2push));
}
