/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   directions.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/07/27 11:59:14 by eteppo        #+#    #+#                 */
/*   Updated: 2021/08/11 09:46:38 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

int	find_direction_to_top(t_list *stack, int location)
{
	if (location == 0 || location == ft_lstsize(stack))
		return (0);
	else if (location <= ft_lstsize(stack) / 2)
		return (1);
	else
		return (-1);
}

void	move_to_biggest_to_top(t_list *b)
{
	int	location;
	int	nb;

	nb = 0;
	location = find_max_and_loc(b, &nb);
	if (find_direction_to_top(b, location) == 1)
	{
		while (location > 0)
		{
			rotate_and_write(b, "rb\n", 1);
			location--;
		}
	}
	else if (find_direction_to_top(b, location) == -1)
	{
		while (location < ft_lstsize(b))
		{
			rev_rotate_and_write(b, "rrb\n", 1);
			location++;
		}
	}
}

void	move_stacks(t_list *a, t_list *b, char *instruction)
{
	if (ft_strcmp(instruction, "rr") == 0)
	{
		rotate_and_write(a, "rr", 0);
		rotate_and_write(b, "rr", 0);
		write(1, "rr\n", 3);
	}
	else if (ft_strcmp(instruction, "rrr") == 0)
	{
		rev_rotate_and_write(a, "rrr", 0);
		rev_rotate_and_write(b, "rrr", 0);
		write(1, "rrr\n", 4);
	}
}

void	update_location(t_nbs *nb, int *location)
{
	if (nb->direction_a == 1)
		(*location)--;
	else if (nb->direction_a == -1)
		(*location)++;
}

void	step_to_direction(t_list *a, t_list *b, t_nbs *nb, int *loc)
{
	if (nb->direction_a == 0 && nb->direction_b == 0)
		return ;
	update_location(nb, loc);
	if (nb->direction_a == nb->direction_b)
	{
		if (nb->direction_a == 1)
			move_stacks(a, b, "rr");
		else if (nb->direction_a == -1)
			move_stacks(a, b, "rrr");
		return ;
	}
	if (nb->direction_a == 1)
		rotate_and_write(a, "ra\n", 1);
	else if (nb->direction_a == -1)
		rev_rotate_and_write(a, "rra\n", 1);
	if (nb->direction_b == 1)
		rotate_and_write(b, "rb\n", 1);
	else if (nb->direction_b == -1)
		rev_rotate_and_write(b, "rrb\n", 1);
}
