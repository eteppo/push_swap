/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   actions.c                                          :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/05/19 17:26:16 by eteppo        #+#    #+#                 */
/*   Updated: 2021/08/12 11:04:31 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	rotate_and_write(t_list *stack, char *instruction, int bool_write)
{
	int		i;
	int		*temp;
	int		size;
	t_list	*last;

	i = 0;
	temp = 0;
	size = ft_lstsize(stack);
	last = NULL;
	if (ft_lstsize(stack) <= 1)
		return ;
	temp = stack->content;
	last = ft_lstlast(stack);
	while (i < size - 1)
	{
		stack->content = stack->next->content;
		stack = stack->next;
		i++;
	}
	last->content = temp;
	if (bool_write == 1)
		write(1, instruction, 3);
}

void	rev_rotate_and_write(t_list *stack, char *instruction, int bool_write)
{
	int		i;
	int		size;
	int		*first;
	int		*second;
	t_list	*last;

	i = 0;
	size = ft_lstsize(stack);
	if (ft_lstsize(stack) <= 1)
		return ;
	first = stack->content;
	last = ft_lstlast(stack);
	stack->content = last->content;
	while (i < size - 1)
	{
		second = stack->next->content;
		stack = stack->next;
		stack->content = first;
		first = second;
		i++;
	}
	if (bool_write == 1)
		write(1, instruction, 4);
}

void	swap_and_write(t_list *stack, char *action)
{
	if (stack && stack->next)
		ft_swap(stack->content, stack->next->content);
	write(1, action, 3);
}

void	push_and_write(t_list **dst, t_list **src, char *action)
{
	t_list	*temp;

	temp = 0;
	if (*src)
	{
		temp = *src;
		*src = (*src)->next;
		ft_lstadd_front(dst, temp);
		*dst = temp;
	}
	write(1, action, 3);
}

int	push_if_correct_spot(t_list **a, t_list **b, int location)
{
	if ((location == 0 || location == ft_lstsize(*a)) && \
	is_correct_spot_to_push(*b, *((int *)(*a)->content)) == 1)
	{
		push_and_write(b, a, "pb\n");
		return (1);
	}
	return (0);
}
