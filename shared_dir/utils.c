/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   utils.c                                            :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/08/09 12:38:27 by eteppo        #+#    #+#                 */
/*   Updated: 2021/08/12 11:05:13 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/shared.h"

void	ft_error(void)
{
	write(2, "Error\n", 6);
	exit(1);
}

void	delete_list(t_list *stack)
{
	t_list	*temp;

	while (stack)
	{
		temp = stack;
		stack = stack->next;
		free(temp->content);
		free(temp);
	}
}

int	is_in_order(t_list *a)
{
	while (a)
	{
		if (a->next)
		{
			if (*((int *)a->content) > *((int *)a->next->content))
				return (0);
		}
		a = a->next;
	}
	return (1);
}

void	ft_swap(int *a, int *b)
{
	int	temp;

	temp = 0;
	if (a && b)
	{
		temp = *b;
		*b = *a;
		*a = temp;
	}
}

int	ft_atoi_mod(const char *str)
{
	long	result;
	int		sign;
	int		i;

	i = 0;
	result = 0;
	sign = 1;
	if (str[i] == '-')
		sign = -1;
	if (str[i] == '+' || str[i] == '-')
		i++;
	while (str[i] >= '0' && str[i] <= '9')
	{
		if ((sign == 1 && result * 10 + (str[i] - '0') > 2147483647)
			|| (sign == -1 && result * 10 + (str[i] - '0') > 2147483648))
			return (0);
		result = result * 10 + (str[i] - '0');
		i++;
	}
	return (result * sign);
}
