/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   argument_checker.c                                 :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/08/09 12:40:01 by eteppo        #+#    #+#                 */
/*   Updated: 2021/08/09 14:40:40 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/shared.h"

int	argument_checker(int argc, char **argv)
{
	int	i;
	int	j;

	i = 1;
	j = 1;
	if (argc < 2)
		return (0);
	while (i <= argc - 1)
	{
		if (ft_atoi_mod(argv[i]) == 0 && ft_strcmp(argv[i], "0") != 0)
			return (-1);
		j = 1;
		while (j < i)
		{
			if (ft_atoi_mod(argv[j]) == ft_atoi_mod(argv[i]))
				return (-1);
			j++;
		}	
		i++;
	}
	return (1);
}
