/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   save_stack.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/08/09 12:39:54 by eteppo        #+#    #+#                 */
/*   Updated: 2021/08/09 13:23:07 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/shared.h"

int	save_stack(int argc, char **argv, t_list **a)
{
	int		i;
	int		*ptr;
	t_list	*new;

	new = NULL;
	i = argc - 1;
	while (i > 0)
	{
		ptr = malloc(sizeof(int));
		if (!ptr)
			return (-1);
		*ptr = ft_atoi_mod(argv[i]);
		new = ft_lstnew((void *)ptr);
		if (!new)
			return (-1);
		ft_lstadd_front(a, new);
		i--;
	}
	return (0);
}
