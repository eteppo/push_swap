/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   instr_rotate.c                                     :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/05/19 17:29:17 by eteppo        #+#    #+#                 */
/*   Updated: 2021/05/25 14:43:55 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/checker.h"

void	ft_rotate(t_list *stack)
{
	int		i;
	int		*temp;
	int		size;
	t_list	*last;

	i = 0;
	temp = 0;
	size = ft_lstsize(stack);
	last = NULL;
	if (ft_lstsize(stack) <= 1)
		return ;
	temp = stack->content;
	last = ft_lstlast(stack);
	while (i < size - 1)
	{
		stack->content = stack->next->content;
		stack = stack->next;
		i++;
	}
	last->content = temp;
}

void	ft_rrotate(t_list *stack)
{
	int		i;
	int		size;
	int		*first;
	int		*second;
	t_list	*last;

	i = 1;
	first = 0;
	second = 0;
	size = ft_lstsize(stack);
	if (ft_lstsize(stack) <= 1)
		return ;
	first = stack->content;
	last = ft_lstlast(stack);
	stack->content = last->content;
	while (i < size)
	{
		if (stack->next)
			second = stack->next->content;
		stack = stack->next;
		stack->content = first;
		first = second;
		i++;
	}
}

void	ft_rotate_stacks(t_list *a, t_list *b, char *str)
{
	if (ft_strcmp(str, "rr") == 0)
	{
		ft_rotate(a);
		ft_rotate(b);
	}
	else if (ft_strcmp(str, "rrr") == 0)
	{
		ft_rrotate(a);
		ft_rrotate(b);
	}
}
