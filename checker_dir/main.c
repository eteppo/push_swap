/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   main.c                                             :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/05/13 13:28:45 by eteppo        #+#    #+#                 */
/*   Updated: 2021/08/09 13:22:24 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/checker.h"

void	order_checking(t_list *a, t_list *b)
{
	if (b)
		write(1, "KO\n", 3);
	else if (ft_lstsize(a) == 1)
		write(1, "OK\n", 3);
	else if (is_in_order(a) == 1)
		write(1, "OK\n", 3);
	else
		write(1, "KO\n", 3);
}

int	instruction_router(char *line, t_list **a, t_list **b)
{
	if (ft_strcmp(line, "sa") == 0)
		ft_swap_one_stack(*a);
	else if (ft_strcmp(line, "sb") == 0)
		ft_swap_one_stack(*b);
	else if (ft_strcmp(line, "ss") == 0)
		ft_swap_two_stacks(*a, *b);
	else if (ft_strcmp(line, "pa") == 0)
		ft_push_stack(a, b);
	else if (ft_strcmp(line, "pb") == 0)
		ft_push_stack(b, a);
	else if (ft_strcmp(line, "ra") == 0)
		ft_rotate(*a);
	else if (ft_strcmp(line, "rb") == 0)
		ft_rotate(*b);
	else if (ft_strcmp(line, "rr") == 0)
		ft_rotate_stacks(*a, *b, "rr");
	else if (ft_strcmp(line, "rra") == 0)
		ft_rrotate(*a);
	else if (ft_strcmp(line, "rrb") == 0)
		ft_rrotate(*b);
	else if (ft_strcmp(line, "rrr") == 0)
		ft_rotate_stacks(*a, *b, "rrr");
	else
		return (-1);
	return (0);
}

int	main(int argc, char **argv)
{
	char		*line;
	int			ret;
	t_list		*a;
	t_list		*b;

	ret = 1;
	a = NULL;
	b = NULL;
	if (argument_checker(argc, argv) == -1)
		ft_error();
	else if (argument_checker(argc, argv) == 0)
		return (0);
	if (save_stack(argc, argv, &a) == -1)
		ft_error();
	while (ret > 0)
	{
		ret = get_next_line(&line);
		if (instruction_router(line, &a, &b) == -1 && ret > 0)
			ft_error();
		free(line);
	}
	order_checking(a, b);
	delete_list(a);
	delete_list(b);
	return (0);
}
