/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   swap_and_push.c                                    :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/05/19 17:26:16 by eteppo        #+#    #+#                 */
/*   Updated: 2021/08/12 11:04:45 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/checker.h"

void	ft_swap_one_stack(t_list *stack)
{
	if (stack && stack->next)
		ft_swap(stack->content, stack->next->content);
}

void	ft_swap_two_stacks(t_list *stack1, t_list *stack2)
{
	if (stack1 && stack1->next)
		ft_swap(stack1->content, stack1->next->content);
	if (stack2 && stack2->next)
		ft_swap(stack2->content, stack2->next->content);
}

void	ft_push_stack(t_list **dst, t_list **src)
{
	t_list	*temp;

	temp = 0;
	if (*src)
	{
		temp = *src;
		*src = (*src)->next;
		ft_lstadd_front(dst, temp);
		*dst = temp;
	}
}
