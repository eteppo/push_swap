/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_lstiter.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/11/19 14:32:09 by eteppo        #+#    #+#                 */
/*   Updated: 2021/05/16 17:43:27 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstiter(t_list *lst, void (*f)(void *))
{
	t_list	*this;

	if (!lst)
		return ;
	this = lst;
	while (this != 0)
	{
		f(this->content);
		this = this->next;
	}
}
