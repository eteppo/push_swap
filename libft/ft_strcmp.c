/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_strcmp.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/08/27 11:22:43 by eteppo        #+#    #+#                 */
/*   Updated: 2020/08/27 14:08:44 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

int	ft_strcmp(char *s1, char *s2)
{
	char	*p1;
	char	*p2;

	p1 = s1;
	p2 = s2;
	while (*p1 != '\0')
	{
		if (*p2 == '\0')
			return (1);
		else if (*p2 > *p1)
			return (-1);
		else if (*p2 < *p1)
			return (1);
		p1++;
		p2++;
	}
	if (*p2 != '\0')
		return (-1);
	return (0);
}
