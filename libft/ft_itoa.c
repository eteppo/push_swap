/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_itoa.c                                          :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/11/11 10:51:53 by eteppo        #+#    #+#                 */
/*   Updated: 2021/05/16 17:42:56 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	nb_len(long nb)
{
	size_t	len;

	len = 0;
	if (nb < 0)
	{
		len++;
		nb = -1 * nb;
	}
	while (nb > 9)
	{
		len++;
		nb = nb / 10;
	}
	if (nb < 10)
		len++;
	return (len);
}

static char	*ft_nb_to_char(int nb, unsigned int len, char *s)
{
	unsigned int	checkpoint;
	long			tempnb;

	checkpoint = 0;
	tempnb = (long)nb;
	if (nb < 0)
	{
		s[0] = '-';
		checkpoint = 1;
		tempnb = -1 * tempnb;
	}
	while (len > checkpoint)
	{
		if (tempnb > 9)
		{
			s[len - 1] = (tempnb % 10) + 48;
			tempnb = tempnb / 10;
		}
		else
			s[len - 1] = tempnb + 48;
		len--;
	}
	return (s);
}

char	*ft_itoa(int nb)
{
	char	*s;
	size_t	len;

	len = nb_len((long)nb);
	s = (char *)malloc(sizeof(char) * (len + 1));
	if (!s)
		return (0);
	s = ft_nb_to_char(nb, len, s);
	s[len] = '\0';
	return (s);
}
