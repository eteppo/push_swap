/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_strrchr.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/11/06 10:35:42 by eteppo        #+#    #+#                 */
/*   Updated: 2021/05/16 17:45:29 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strrchr(const char *s, int c)
{
	int	len;

	len = 0;
	while (s[len] != '\0')
		len++;
	if ((char)c == '\0' && s[len] == '\0')
		return (&((char *)s)[len]);
	while (len > 0)
	{
		if (s[len - 1] == (char)c)
			return (&((char *)s)[len - 1]);
		else
			len--;
	}
	return (0);
}
