/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_memcmp.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/08/27 14:16:36 by eteppo        #+#    #+#                 */
/*   Updated: 2020/11/21 17:44:54 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_memcmp(const void *s1, const void *s2, size_t n)
{
	size_t	i;

	i = 0;
	while (i < n)
	{
		if (((unsigned const char *)s1)[i] != ((unsigned const char *)s2)[i])
		{
			return (((unsigned const char *)s1)[i]
			- ((unsigned const char *)s2)[i]);
		}
		i++;
	}
	return (0);
}
