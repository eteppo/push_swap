/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_atoi.c                                          :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/08/23 10:49:15 by eteppo        #+#    #+#                 */
/*   Updated: 2021/05/16 17:42:17 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

static int	is_whitespace(const char *str, int i)
{
	return (str[i] == ' ' || str[i] == '\t' || str[i] == '\n'
		|| str[i] == '\v' || str[i] == '\f' || str[i] == '\r');
}

int	ft_atoi(const char *str)
{
	long	result;
	int		sign;
	int		i;

	i = 0;
	result = 0;
	sign = 1;
	while (is_whitespace(str, i))
		i++;
	if (str[i] == '-')
		sign = -1;
	if (str[i] == '+' || str[i] == '-')
		i++;
	while (str[i] >= '0' && str[i] <= '9')
	{
		if (sign == 1 && result * 10 + (str[i] - '0') > 2147483647)
			return (-1);
		else if (sign == -1 && result * 10 + (str[i] - '0') > 2147483648)
			return (0);
		result = result * 10 + (str[i] - '0');
		i++;
	}
	return (result * sign);
}
