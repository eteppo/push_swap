/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_strndup.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/20 11:00:44 by eteppo        #+#    #+#                 */
/*   Updated: 2021/01/20 11:01:12 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strndup(const char *s1, size_t n)
{
	char	*ptr;

	ptr = (char *)malloc(sizeof(char) * (n + 1));
	if (!ptr)
		ptr = NULL;
	else
	{
		ft_memcpy(ptr, s1, n);
		ptr[n] = '\0';
	}
	return (ptr);
}
