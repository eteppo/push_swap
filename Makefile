# **************************************************************************** #
#                                                                              #
#                                                         ::::::::             #
#    Makefile                                           :+:    :+:             #
#                                                      +:+                     #
#    By: eteppo <eteppo@student.codam.nl>             +#+                      #
#                                                    +#+                       #
#    Created: 2021/05/13 13:21:34 by eteppo        #+#    #+#                  #
#    Updated: 2021/08/12 11:04:15 by eteppo        ########   odam.nl          #
#                                                                              #
# **************************************************************************** #

PUSH_SWAP = push_swap
CHECKER = checker
NAME = $(CHECKER) $(PUSH_SWAP)
SRCS_LIST_C =	main.c \
				rotate.c \
				swap_and_push.c
SRCS_LIST_P =	main.c \
				utils.c \
				actions.c \
				directions.c \
				find_location.c \
				location_utils.c \
				find_spot_to_push.c
SRCS_LIST_S =	argument_checker.c \
				save_stack.c \
				utils.c
SRCS_C = $(addprefix checker_dir/,${SRCS_LIST_C})
SRCS_P = $(addprefix push_swap_dir/,${SRCS_LIST_P})
SRCS_S = $(addprefix shared_dir/,${SRCS_LIST_S})
OBJS_C = $(SRCS_C:.c=.o)
OBJS_P = $(SRCS_P:.c=.o)
OBJS_S = $(SRCS_S:.c=.o)
HEADER_FILES = includes
LIBRARY_LIBFT = libft/libft.a
CC = gcc
CFLAGS = -Wall -Wextra -Werror

all: $(NAME)

$(NAME): $(OBJS_C) $(OBJS_P) $(OBJS_S) $(LIBRARY_LIBFT)
	$(CC) $(OBJS_C) $(OBJS_S) $(LIBRARY_LIBFT) -o $(CHECKER)
	$(CC) $(OBJS_P) $(OBJS_S) $(LIBRARY_LIBFT) -o $(PUSH_SWAP)

$(LIBRARY_LIBFT):
	$(MAKE) -C libft bonus

%.o: %.c $(HEADER_FILES)
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm -f $(OBJS_C) $(OBJS_P) $(OBJS_S)
	$(MAKE) -C libft clean

fclean: clean
	rm -f $(CHECKER) $(PUSH_SWAP) libft/libft.a

re: fclean all

.PHONY: all clean fclean re
